###################################
## WORKSPACE SETUP AND STANDARD MODULE IMPORT
###################################

# standard python modules
import re # regular expressions
import imp
import inspect
import math
import numpy as np
import copy
import time
from datetime import datetime

# Directories and Solving
wd = os.path.dirname(os.path.realpath('__file__'))  # Getting working directory of Abaqus
os.chdir(wd)  # Change to this directory
ncpus = 2 # Number of cpus to use in simulations

###################################
## ABAQUS MODULE IMPORT AND SETUP
###################################
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *

from abaqus import *
from abaqusConstants import *

# allows us to use findat in journal file
session.journalOptions.setValues(replayGeometry=COORDINATE, recoverGeometry=COORDINATE)

# make a new Model Database
Mdb()

###################################
## CUSTOM MODULE IMPORT
###################################
import deamodel
import membrane

# unit system = mm, mN, kPa, V

###################################
## GEOMETRY DEFINITIONS
###################################
length = 10                 # side length, mm
height = 1                  # total membrane thickness, mm
real_layer_height = 0.1     # layer height as manufactured IRL
# desired_num_layers = 3

membrane_geom = membrane.SquareMembraneGeometry(length, height, real_layer_height)
membrane_part = membrane.MembranePart(membrane_geom, 'membrane_part')

###################################
## MATERIAL DEFINITIONS
###################################
YM = 210       # young's modulus, kPa
mu = YM/3       # shear modulus, kPa
eps_rel = 2.9   # relative permittivity
membrane_mat = deamodel.materials.Hyperelastic('Wacker_P7670', mu, eps_rel) # calculate all remaining params and make in abaqus

###################################
## INPUTS
###################################
voltage = 2500 # voltage in V

###################################
## DEVICE SETUP
###################################
device = membrane.Device(membrane_part, membrane_mat, voltage)

###################################
## MODEL SETUP
###################################
# create our new model and delete the default model-1
model_name = 'membrane_test'
model = mdb.Model(modelType=STANDARD_EXPLICIT, name=model_name)
del mdb.models['Model-1']
membrane_model = membrane.SimpleMembrane(device, model)

# generate parts, section, apply constraints, etc
membrane_model.set_up_model()

# make sure we can see what we're interested in
session.viewports['Viewport: 1'].setValues(displayedObject=membrane_model.part.abq)

# create job file with custom UELs
job = membrane_model.set_up_job()

# Run the job
job.submit()
job.waitForCompletion()

# analyze and export the results of the simulation
odb_name = wd+'/'+job.name+'.odb'
odb = session.openOdb(name=odb_name)
disp_x, disp_z = membrane_model.measure(odb)
print( 'X-displacement: %.1f um (%.1f%% strain)' % (disp_x*1000, 100*disp_x/length) )
print( 'Z-displacement: %.1f um (%.1f%% strain)' % (disp_z*1000, 100*disp_z/height) )
