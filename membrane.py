import deamodel
from deamodel.abqmodel import AbqModel
from deamodel.selection import *
from deamodel.sectioning import *

from abaqus import *
from abaqusConstants import *

from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *

class SquareMembraneGeometry:
    """ Geometry definition of an LxLxH square prism """

    def __init__(
        self,
        length,                     # length of membrane, mm
        height,                     # height of membrane, mm
        real_layer_height,          # real manufactured layer height of active DEA layers
        desired_num_layers = None   # the number of active DEA layers
    ):
        self.length = length
        self.height = height
        self.real_layer_height = real_layer_height
        self.desired_num_layers = desired_num_layers

class MembranePart:
    """ Instructions for building the membrane from geometry specifications """

    def __init__(
        self,
        geom,                       # geometric specifications of the part
        name = 'membrane'       # name of the part to generate from this geometry
    ):
        self.geom = geom
        self.name = name

    def build(self, model):
        """ Build a part from the geometry in the current model """
        # make the sketch
        sketch = model.ConstrainedSketch(name='__profile__', sheetSize=100.0)
        sketch.rectangle(point1=(0.0, 0.0), point2=(self.geom.length, self.geom.length))
        abq_part = model.Part(dimensionality=THREE_D, name=self.name,
            type=DEFORMABLE_BODY)

        # extrude the sketch
        abq_part.BaseSolidExtrude(depth=self.geom.height, sketch=sketch)
        del sketch

        # make references to useful abaqus features
        self.abq = abq_part
        self.membrane_cells = selection.pick_cells(self.abq, [(0,0,0)])

        left_pt = (0.0, self.geom.length/2.0, self.geom.height/2.0)
        right_pt = (self.geom.length, self.geom.length/2.0, self.geom.height/2.0)
        bottom_pt = (self.geom.length/2.0, 0.0, self.geom.height/2.0)
        top_pt = (self.geom.length/2.0, self.geom.length, self.geom.height/2.0)
        back_pt = (self.geom.length/2.0, self.geom.length/2.0, 0.0)
        front_pt = (self.geom.length/2.0, self.geom.length/2.0, self.geom.height)

        selection.group_faces(self.abq, [left_pt], 'LEFT-FACE')
        selection.group_faces(self.abq, [right_pt], 'RIGHT-FACE')
        selection.group_faces(self.abq, [bottom_pt], 'BOTTOM-FACE')
        selection.group_faces(self.abq, [top_pt], 'TOP-FACE')
        selection.group_faces(self.abq, [back_pt], 'BACK-FACE')
        selection.group_faces(self.abq, [front_pt], 'FRONT-FACE')

        return abq_part

    def laminate(self, model):
        """ Split the membrane into layers for applying voltage """
        centroid = (self.geom.length/2, self.geom.length/2, self.geom.height/2) # centroid of prism
        membrane_cells = selection.pick_cells(self.abq, [centroid])
        # If you want a specific number of active layers to be used to make
        # the simulation run faster (scaling up the layer thickness from the
        # real layer thickness) then you can specify fewer active layers and allow
        # this function to figure out the appropriate scaling factor for voltage
        # and layer thickness
        if self.geom.desired_num_layers is None:
            self.geom.layer_scaling = 1.0
        else:
            self.geom.layer_scaling = deamodel.sectioning.calc_layer_scaling(
                layer_height=self.geom.real_layer_height,
                distance=self.geom.height,
                desired_num=self.geom.num_active_layers,
                active_ends = [True, True])

        scaled_layer_height = self.geom.real_layer_height*self.geom.layer_scaling

        # Get the odd and even faces (odd and even electrodes) for applying voltage
        odd_faces, even_faces, membrane_set = deamodel.sectioning.laminate(
            type='cartesian', part=self.abq, cells=membrane_cells,
            starting_pt=(self.geom.length/2, self.geom.length/2, 0),
            direction=(0,0,1), distance=self.geom.height,
            layer_height=scaled_layer_height,
            endcap_height=self.geom.real_layer_height,
            active_ends=[True, True],
            name='membrane')

        self.even_faces = even_faces
        self.odd_faces = odd_faces

    def mesh(self, model):
        """ Mesh the part """
        self.abq.seedPart(deviationFactor=0.1, minSizeFactor=0.1, size=self.geom.height)
        self.abq.setElementType(elemTypes=(
            ElemType(elemCode=C3D8RH, elemLibrary=STANDARD),
            ElemType(elemCode=C3D6, elemLibrary=STANDARD),
            ElemType(elemCode=C3D4, elemLibrary=STANDARD)),
            regions=self.abq.sets['membrane_cells'])
        self.abq.generateMesh()

    def place(self, model, instance_name, translation=(0,0,0)):
        """ Place an instance of this part in the assembly """
        part_instance = model.rootAssembly.Instance(dependent=ON, name=instance_name, part=self.abq)
        # part_instance.translate(vector=translation)
        # TODO: add translation function and update rest of code to match

        return part_instance

class Device:
    def __init__(
        self,
        membrane_part,
        membrane_mat,
        initial_voltage = 0
    ):
        self.membrane_part = membrane_part
        self.membrane_mat = membrane_mat
        self.voltage = initial_voltage

class SimpleMembrane(AbqModel):
    """ We create a class to describe the entire model we are going to build in
    Abaqus. To make our lives easier, we inherit from AbqModel """

    def __init__(self, device, model):
        AbqModel.__init__(self, model)
        self.part = device.membrane_part
        self.membrane_mat = device.membrane_mat
        self.membrane_mat.to_abaqus(self.model)
        self.voltage = device.voltage

    def apply_voltage(self, voltage, part_instance, step_name='Initial'):
        """ Apply the specified voltage to even electrodes """
        # update the voltage
        self.voltage = voltage
        # even faces = +kV
        self.model.TemperatureBC(createStepName=step_name,
            distributionType=UNIFORM, fieldName='', magnitude=voltage,
            name='phi_applied', region=part_instance.sets['membrane_even_faces'])
        # odd faces = GND
        self.model.TemperatureBC(createStepName=step_name,
            distributionType=UNIFORM, fieldName='', magnitude=0.0,
            name='phi_ground', region=part_instance.sets['membrane_odd_faces'])

    def constrain(self, instance):
        """ Apply mechanical constraints/boundary conditions to the model
        In this case we apply symmetric constraints to simulate a freestanding,
        biaxially-stretched membrane, a case which can be easily modeled
        analytically """
        self.model.XsymmBC( createStepName='Initial', localCsys=None,
            name='x-symm', region=instance.sets['LEFT-FACE'] )
        self.model.YsymmBC( createStepName='Initial', localCsys=None,
            name='y-symm', region=instance.sets['BOTTOM-FACE'] )
        self.model.ZsymmBC( createStepName='Initial', localCsys=None,
            name='z-symm', region=instance.sets['BACK-FACE'] )

    def create_step(self, step_name, previous='Initial'):
        self.model.CoupledTempDisplacementStep(deltmx=1.0, initialInc=0.1,
            maxInc=0.1, name=step_name, nlgeom=ON, previous=previous,
            amplitude=RAMP, response=STEADY_STATE) # make sure we use ramp and steady state

    def generate_headline(self, comma_separated=False):
        """ Generate a 'headline', a string for quickly reading key parameters of a simulation
        This headline is used for a variety of functions such as naming jobs
        and files """
        geom = self.part.geom
        headline = 'simple_membrane-' + \
            'L{:.2g}'.format(geom.length) + \
            '_H{:.2g}'.format(geom.height) + \
            '_E{:.0f}'.format(self.voltage/(geom.real_layer_height*1000)) # electric field in V/um or kV/mm

        # job_name can't contain '.', as happens with floats,
        # so use this option for job naming
        if comma_separated:
            headline = headline.replace('.', ',')

        return headline

    def measure(self, odb, step_name='actuation'):
        """Measure the displacement of the membrane"""
        # extract all displacements in the last frame
        frame = odb.steps[step_name].frames[-1]
        displacement = frame.fieldOutputs['U']

        # get the in-plane displacements from the right face
        right_nodes = odb.rootAssembly.instances['MEMBRANE-1'].nodeSets['RIGHT-FACE']
        disp_x = displacement.getSubset(region=right_nodes)
        disp_x = np.array([disp_x.values[ii].data for ii in range(len(right_nodes.nodes))])
        disp_x = np.mean(disp_x, axis=0)
        disp_x = disp_x[0]

        # get the out-of-plane displacements from the top
        front_nodes = odb.rootAssembly.instances['MEMBRANE-1'].nodeSets['FRONT-FACE']
        disp_z = displacement.getSubset(region=front_nodes)
        disp_z = np.array([disp_z.values[ii].data for ii in range(len(front_nodes.nodes))])
        disp_z = np.mean(disp_z, axis=0)
        disp_z = disp_z[2]

        return disp_x, disp_z

    def section(self):
        """ Create and assign a section for the membrane material """
        geom = self.part.geom
        centroid = (geom.length/2, geom.length/2, geom.height/2) # centroid of prism
        membrane_cells = selection.group_cells(self.part.abq, [centroid], 'membrane_cells')
        membrane_section = Section('membrane_section', self.membrane_mat,
            membrane_cells, self.part.abq, self.model)

    def set_up_job(self):
        job_name = self.generate_headline(comma_separated=True)
        return AbqModel.set_up_job(self, job_name, self.membrane_mat.eps)

    def set_up_model(self):
        """ Set up everything in the model (excludes job setup)
        This method calls every other relevant method
        """
        # build, section, laminate, and mesh the part
        part = self.part.build(self.model)
        self.section()
        self.part.laminate(self.model)
        self.part.mesh(self.model)
        # place the part into the assembly
        part_instance = self.part.place(self.model, 'membrane-1')
        # assign boundary conditions (voltage and mechanical constraints)
        step_name = 'actuation'
        self.create_step(step_name)
        self.apply_voltage(self.voltage*self.part.geom.layer_scaling, part_instance, step_name)
        self.constrain(part_instance)
